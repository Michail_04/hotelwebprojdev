<?php

Route::post('/login', 'AuthController@login');

    Route::group(['middleware' => ['jwt.auth']], function () {

        Route::group(['middleware' => ['InAuth']], function () {
        
                Route::post('/refreshtoken', 'AuthController@refreshToken');
                            
                Route::group(['middleware' => 'iAdmin'], function () {

                    Route::get('admin/showrooms/{free?}/{type?}/{person?}', 'RoomsController@showrooms');       
                    Route::post('admin/showrooms/filter', 'RoomsController@filter');
                    Route::post('admin/showrooms/editroms', 'RoomsController@showEditRoom');
                    Route::post('admin/editroom', 'RoomsController@editRoom');
                    Route::post('admin/room/createroom', 'RoomsController@createroom');
                    Route::post('admin/room/delete', 'RoomsController@delete');

                    Route::get('admin/showusers', 'AdminController@showusers');
                    Route::post('admin/showusers/filter', 'AdminController@filter');
                    Route::post('admin/showusers/edituser', 'AdminController@showedituser');
                    Route::post('admin/edituser', 'AdminController@edituser');
                    Route::post('admin/createuser', 'AdminController@createuser');
                    Route::post('admin/user/delete', 'AdminController@delete');

                    Route::get('admin/showservice', 'ServiceController@show');
                    Route::post('admin/showservice/edit', 'ServiceController@edit');
                    Route::post('admin/createservise', 'ServiceController@create');
                    Route::post('admin/editservise', 'ServiceController@showedit');

                });


            Route::get('showfreerooms/{free}/{person}', 'RoomsController@showfreerooms');     

            Route::get('showroomers', 'RoomerController@showroomers');
            Route::get('showroomers/romerinfo/{id}/{flag?}', 'RoomerController@showroomerinfo');
            Route::post('roomer/saveroomer', 'RoomerController@saveroomer');
            Route::post('roomer/editroomer', 'RoomerController@editroomer');
            Route::post('roomer/delete', 'RoomerController@delete');

            Route::get('showdate/{free}/{type}/{person}/{date?}/{flag?}', 'DateController@getdate');
        });
    });

?>
