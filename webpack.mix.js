const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .scripts('node_modules/material-design-lite/material.js','public/js/material.js')
    .styles('node_modules/material-design-lite/material.css','public/css/material.css')
    .scripts('node_modules/vue-material/dist/vue-material.js','public/js/vue-material.js')
    .styles('node_modules/vue-material/dist/vue-material.css','public/css/vue-material.css')
    
