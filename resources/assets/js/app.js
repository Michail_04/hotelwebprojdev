import Vue from 'vue'
import VueRouter from 'vue-router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import VueProgressBar from 'vue-progressbar'
import App from './App.vue';

import Login from './views/login'
import Admin from './views/admin'
import Roomer from './views/roomer'

import Auth from './store/auth'
import Calendar from './views/calendar'
import { post } from './helpers/axios'
import { get } from './helpers/axios'
import moment from 'moment'




      

Vue.use(VueRouter)
Vue.use(VueMaterial)

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '4px'
})

Vue.material.registerTheme({
  green: {
    primary: 'green',
    accent: 'orange',
    warn: 'red',
    background: 'grey'
  }
})


var router = new VueRouter({
  routes: [
    { path: '/', component: Calendar },
    { path: '/login', component: Login },
    { path: "/admin", component: Admin },
     { path: "/roomer", component: Roomer }
  ]
})

const app = new Vue({
  el: '#app',
  data() {
    return {
      auth: Auth.state,

    }
  },
  filters: {
    formatDate: function (date) {
      return moment(String(date)).format('MM/DD/YYYY hh:mm');
    }
  },
  template: '<app></app>',
  components: { App },
  router: router
})

