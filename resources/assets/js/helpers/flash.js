export default{
    state:{

        success: null,

        error: null,

        message: null,
        
    },
setSuccess(message,messageSuccess=null){
    this.state.success = message
    this.state.message = messageSuccess
    setTimeout(()=>{
        this.removeSucces()
    }, 3000)
},
setError(message, errorMsg=null, error){
    this.state.error = message
    this.state.message = errorMsg
    setTimeout(()=>{
        this.removeError()
    }, 3000)
},
setTokenError(error){
    this.state.tokenEror = error
},
removeSucces(){
    this.state.success = null
    this.state.message = null
},
removeError(){
    this.state.error = null
    this.state.message = null
}

}