<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
        public $timestamps = false;
      protected $fillable = [
        'id','number', 'type','free'
    ];

    public function roomer()
  {
    return $this->hasMany('App\Roomer','number','number');
  }
}
