<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomer extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'id','name', 'passport', 'address','coment','phone','number','value_cost','date_in','date_out'
    ];

}
