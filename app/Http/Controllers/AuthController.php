<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Carbon\Carbon;

class AuthController extends Controller
{

    public function refreshToken(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
            $user = JWTAuth::parseToken()->toUser();
            $now = Carbon::now("Europe/Moscow");
            $refreshdata = $user->refreshDatet;
         
        if ($refreshdata) {
            $refreshdata = Carbon::parse($refreshdata, "Europe/Moscow");
            $hourse =  $refreshdata->diffInHours($now);


            if ($hourse  >= 11) {
                $newToken = JWTAuth::parseToken()->refresh();
                $user->refreshDatet = Carbon::now("Europe/Moscow");
                $user->save();
                return response()->json([
                'auth' => true,
                'refreshToken'=> $newToken,
                'user_id' => $user->id,
                'user_name' => $user->name,
                'rules' => $user->rules,
                'time'=>$user->refreshDatet
                ], 200);
            } else {
                $refreshdata->addHours(10);/*addMinutes()->addSeconds(20);*/
                $arrtime = $now->diffInSeconds($refreshdata);
                return response()->json(['timeErr' =>  $arrtime], 200);
            }
        }

            return response()->json(['parsDataFromBd'=>'time-parsBD-error'], 422);
    }


    public function login(Request $request)
    {


        $this->validate($request, [
          'email'=>'required|email',
          'password'=>'required'
        ],
        [
        'required' => 'Поле :attribute должно быть заполнено',
        'email' => 'Невернный формат поля :attribute'
        ]);

 



        $user =  User::where('email', $request->email)
        ->first();
        
        $credentials = $request->only('email', 'password');
      
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                'autherror'=>'Неверный email или password!'
                ], 422);
            }
        } catch (JWTException $e) {
            return response()->json([
            'error'=>'Ошибка авторизации(T_UNC)'
            ], 422);
        }

        $user->refreshDatet = Carbon::now("Europe/Moscow");
        $user->save();
        
          return response()->json([
          'auth' => true,
          'token' =>$token,
          'user_id' => $user->id,
          'user_name' => $user->name,
          'rules' => $user->rules
          ], 200);
    }
}
