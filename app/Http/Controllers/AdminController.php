<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use \App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AdminController extends Controller
{
    private $editParam = array();
    

    public function delete(Request $request)
    {

        if ($request->deleteUsersId) {
            $arrayWithUserId = $request->deleteUsersId;
            DB::table('users')->whereIn('id', $arrayWithUserId)->delete();
             return response()->json(['delete'=> "Пользователь удален!"]);
        } else {
            return response()->json(['deleteerror'=>'Нет данных']);
        }
    }

    public function rename($users)
    {
        foreach ($users as $user) {
            if ($user->rules == 0) {
                $user->rules = ["Администратор гостиницы",0];
            }
            if ($user->rules == 1) {
                $user->rules = ["Администратор системы",1];
            }
        }
        return $users;
    }

    public function editUser(Request $request)
    {
        
         $this->validate($request, [
        'name'=>'required',
         ],
         [
         'required' => 'Поле :attribute должно быть заполнено',
         ]);


        $id = $request->id;
        $user = DB::table('users')->where('id', $id)->get();


        if ($request->email != $user[0]->email) {
             $this->validate($request, [
            'email'=>'required|email|unique:users',
             ],
             [
             'required' => 'Поле :attribute должно быть заполнено',
             'email' => 'Невернный формат поля :attribute',
             'unique'=> 'Данный адресс занят',
             ]);
        }
            
        if ($request->password && $request->repassword) {
            $this->validate($request, [
            'password'=>'min:8',
            'repassword'=>'min:8',
            ],
            ['min' => 'Недостаточно длинный пароль']);
            if ($request->password != $request->repassword) {
                    return response()->json([
                    'pass' => 'Пароль не совпадает',
                    ], 422);
            }

            $this->editParam['password'] =  bcrypt($request->password);
        } else {
              $this->editParam['password'] = $user[0]->password;
        }
       
        $this->editParam['email'] = $request->email;
        $this->editParam['name'] = $request->name;


        if ($request->rules[1] == 0 || $request->rules[1] == 1) {
              $this->editParam['rules'] = $request->rules[1];
        }
        
        if ($request->resAuth) {
              $this->editParam['refreshDatet'] = null;
        } else {
            $this->editParam['refreshDatet'] = $user[0]->refreshDatet;
        }

        DB::table('users')->where('id', $user[0]->id)->update($this->editParam);

        
        return response()->json(["edit" => "Данные обновленны","user"=>$user[0]->email], 200);
    }

    public function createUser(Request $request)
    {

        $this->validate($request, [
        'name'=>'required',
        'email'=>'required|email|unique:users',
        'password'=>'required|min:8',
        'repassword'=>'required|min:8',
        'rules'=>'required',
        ],
        [
        'required' => 'Поле :attribute должно быть заполнено',
        'email' => 'Невернный формат поля :attribute',
        'unique'=> 'Данный адресс занят',
        'min' => 'Недостаточно длинный пароль'
        ]);

        if ($request->password != $request->repassword) {
            return response()->json([
            'pass' => 'Пароль не совпадает',
            ], 422);
        }

        try {
            $user = new User;

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->rules = $request->rules;

            $user->save();

            $newUser = User::all('name', 'email', 'rules', 'refreshDatet')
            ->where('email', $request->email);
        } catch (Exeption $e) {
            return response()->json([
            'create'=>false
            ], 422);
        }
            return response()->json([
            'create'=>true,
            'user'=>$newUser
            ], 201);
    }

    public function showUsers()
    {
        $users = User::all('id', 'name', 'email', 'rules', 'refreshDatet');

        $users = $this->rename($users);
        
        return response()->json(['users'=>$users], 201);
    }
    public function showEditUser(Request $request)
    {
        if ($request->UsersId) {
            $UsersId = $request->UsersId;
            $users = DB::table('users')->select('id','name', 'email', 'rules', 'refreshDatet')->whereIn('id', $UsersId)->get();

            $users = $this->rename($users);
        
            return response()->json(['editusers'=>$users]);
        }
        return response()->json(['editerror'=>"Нет данных"], 422);
    }

    public function filter(Request $request)
    {
        $value =  $request->value;
        if ($value ==  0 || $value == 1) {
            $users = User::all('id', 'name', 'email', 'rules', 'refreshDatet')
            ->where('rules', $value);
            $users = $this->rename($users);
            return response()->json(['users' => $users]);
        }
        if ($value == 2) {
            $users = DB::table('users')->select('id', 'name', 'email', 'rules', 'refreshDatet')->whereNotNull('refreshDatet')->get();
             $users = $this->rename($users);
             return response()->json(['users' => $users]);
        }
        if ($value == 3) {
            $users = User::all('id', 'name', 'email', 'rules', 'refreshDatet');

            $users = $this->rename($users);
            return response()->json(['users' => $users]);
        }
    }
}
