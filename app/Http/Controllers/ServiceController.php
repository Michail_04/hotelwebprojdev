<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Service;
use Centrifugo;

class ServiceController extends Controller
{
    private  $time = null;

    public function show(){
        $services = Service::all()->toArray();
        return response()->json(['services'=>$services]);
    }

    public function create(Request $req){

         $this->validate($req, [
          'name'=>'required',
          'cost'=>'required|numeric'
           ],
           [
           'required' => 'Поле :attribute должно быть заполнено',
           'numeric'=>'Неверный формат'
           ]);


        $service = new Service();
        $service->name = $req->name;
        $service->cost = $req->cost;
        $service->count = 1;
        $service->show = 0;
        $service->costChange = $req->cost;
        
        try{
            $service->save();
            return response()->json(['saveSevice'=>true]);
        }catch(Exeption $e){
            return response()->json(['saveSevice'=>false]);
        }
        

    

    }

    public function edit(Request $req){
       $this->validate($req, [
          'name'=>'required',
          'cost'=>'required|numeric'
           ],
           [
           'required' => 'Поле :attribute должно быть заполнено',
           'numeric'=>'Неверный формат'
           ]);

           $service = Service::where('id',$req->id)->first();
           if($service->name != $req->name){
               $service->name = $req->name;
           }
           if($service->cost != $req->cost){
               $service->cost = $req->cost;
            $service->costChange = $req->cost;
           }
            try{
            $service->save();
            return response()->json(['editSevice'=>true]);
            }catch(Exeption $e){
                return response()->json(['editSevice'=>false]);
            }
    }
    
    public function showedit(Request $req){
        try{
            $services = Service::whereIn('id',$req->ServiceId)->get()->toArray();
            return response()->json(['editservice'=>$services]);
        }catch(Exeptiob $e){
            return response()->json(['editservice'=>false]);
        }
        
        
    }
}
