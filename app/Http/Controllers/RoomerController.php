<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Roomer;
use App\Room;

use DB;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;

class RoomerController extends Controller
{
    private $roomerKey;
     private $Months = array(
        1=>'Января',
        2=>'Февраля',
        3=>'Марта',
        4=>'Апреля',
        5=>'Мая',
        6=>'Июня',
        7=>'Июля',
        8=>'Авгуса',
        9=>'Сентября',
        10=>'Октября',
        11=>'Ноября',
        12=>'Декабря'
    );
    private $DaysOfWeek = array(
        0=>array('Вc',1),
        1=>'Пн',
        2=>'Вт',
        3=>'Ср',
        4=>'Чт',
        5=>'Пт',
        6=>array('Сб',1)
    );

    public function showRoomers()
    {
        $roomers = Roomer::all();
        
        foreach ($roomers as $roomer) {
            try {
                $roomer->phone = decrypt($roomer->phone);
                $roomer->passport = decrypt($roomer->passport);
                $roomer->address = decrypt($roomer->address);
            } catch (DecryptException $e) {
                return response()->json(['er'=>$e]);
            }
        }


        return response()->json(["roomers"=>$roomers]);
    }
    public function showroomerinfo($id, $flag = null)
    {
        setlocale(LC_TIME, "en_EN");
        $dateOut = null;
        $dateIn = null;
        $money = null;
        $number = null;
        $room = null;
        $dateSum = null;
        $dateInTime = null;
        $dateOutTime = null;
                 
        $roomer = Roomer::where('id', $id)->get();
        if ($flag != null) {
            foreach ($roomer as $value) {
                try {
                     $value->phone = decrypt( $value->phone);
                     $value->passport = decrypt( $value->passport);
                     $value->address = decrypt( $value->address);
                     $dateOut = Carbon::parse($value->date_out);
                     $dateIn = Carbon::parse($value->date_in);
                     $money = $value->value_cost;
                     $number = $value->number;
                } catch (DecryptException $e) {
                    return response()->json(['er'=>$e]);
                }
            }
            $room = Room::where('number', $number)->first();
            $dateSum = $dateOut->diffInDays($dateIn);
            if($dateSum == 0){
                 $dateSum = 1;
            }
            $money = $room->cost * $dateSum -  $money;

            $dateInTime = Carbon::parse($roomer[0]->date_in);
            $dateInTime = $dateInTime->format("H:i");
            $dateOutTime = Carbon::parse($roomer[0]->date_out);
            $dateOutTime = $dateOutTime->format("H:i");
            
            $date = $this->formatDate($dateIn,$dateOut);
          

            $roomer[0]->date_in =  array($date[0],$dateIn->toDateString());
            $roomer[0]->date_out = array($date[1],$dateOut->toDateString());
        }
      
        return response()->json([$roomer[0],"money" =>$money,"room"=> $room,"dateSum"=> $dateSum,
        "date_in_time"=>$dateInTime,"date_out_time"=>$dateOutTime]);

    }

    private function formatDate($dateIn,$dateOut){

        $date_in_string = null;
        $date_out_string = null;
        $all_date = null;  

          foreach($this->DaysOfWeek as $key=>$val){

                if($dateIn->format("w") == $key){
                    if(is_array($val)){
                        $date_in_string = $val[0];
                    }else{
                        $date_in_string = $val;
                    }
                }

                if($dateOut->format("w") == $key){
                    if(is_array($val)){
                        $date_out_string = $val[0];
                    }else{
                        $date_out_string = $val;
                    }
                }

            }

            $date_in_string .= " " . $dateIn->format('d');
            $date_out_string .= " " . $dateOut->format('d');

            foreach($this->Months as $key => $val){

                if($key == $dateIn->month){
                    $date_in_string .= " " .  $val;
                }

                if($key == $dateOut->month){
                    $date_out_string .= " " .  $val;
                }

            }

             $date_in_string .= " " . $dateIn->year;
             $date_out_string .= " " . $dateIn->year;

             $all_date = array($date_in_string,$date_out_string);

             return $all_date;
    }
    public function checkDate($date_in, $number, $edit = null)
    {
        $dateOutRoomer;
        $dateInNewRoomer;
        $roomer;
        $roomers = Roomer::where('number', $number)->get();
        if (count($roomers) != 0) {
                $roomer = $roomers->last();
                $dateOutRoomer = Carbon::parse($roomer->date_out);
                $dateInNewRoomer = Carbon::parse($date_in);
                    
            $diff = $dateOutRoomer->diffInSeconds($dateInNewRoomer, false);
            if ($diff>0) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }


    public function saveRoomer(Request $request)
    {
     
        $check = $this->checkDate($request->date_in, $request->room);

       
       
        if ($check == true) {
            $this->validate($request, [
            'name'=>'required',
            'passport'=>'required|unique:roomers,passport',
            'address'=>'required',
            'coment'=>'max:70',
            'phone'=>'required',
            'room' => 'required',
            'date_in'=> 'required',
            'date_out'=> 'required',
            ],
            [
            'required' => 'Поле :attribute должно быть заполнено',
            'email' => 'Невернный формат поля :attribute',
            'unique'=> 'Данный номер паспорта есть занят',
            'max' => 'Слишком большой комментарий'
            ]);
              
            $date_in = Carbon::parse($request->date_in);
            $date_out = Carbon::parse($request->date_out);

            $diff = $date_out->diffInSeconds($date_in, false);

            if ($diff > 0) {
                 return response()->json([
                'saveRoomerError'=>"Дата въезда больше даты выезда!",
                 ], 422);
            }


            $date_in = $date_in->toDateTimeString();
            $date_out = $date_out->toDateTimeString();

            $room = Room::where('number', $request->room)->get();

                  

            
            if (count($room) != 0) {
                Room::where('number', $request->room)->update(['free'=>1]);
            } else {
                return response()->json([
                'saveRoomerError'=>"комнаты с таким номером нет",
                ], 422);
            }

            
            try {
                  $roomer = new Roomer;
                   
                  $roomer->name = $request->name;
                  $roomer->coment = $request->coment;
                  $roomer->phone = encrypt($request->phone);
                  $roomer->passport = encrypt($request->passport);
                  $roomer->address =  encrypt($request->address);
                  $roomer->number = $request->room;
                  $roomer->value_cost = $request->value_cost;
                  $roomer->date_in = $date_in;
                  $roomer->date_out = $date_out;

                  $roomer->save();
            } catch (Exeption $e) {
                return response()->json([
                'saveRoomerError'=>"Ошибка сервера!"
                ], 422);
            }
            return response()->json([
            'saveRoomer'=>true,
            ], 201);
        }
        return response()->json([
        'saveRoomerError'=>"Дата въезда новог клиента меньше даты выезда предыдущего!",
        ], 422);
    }

    public function editroomer(Request $request)
    {
        
         $this->validate($request, [
            'name'=>'required',
            'passport'=>'required|unique:roomers,passport',
            'address'=>'required',
            'coment'=>'max:70',
            'phone'=>'required',
            'number' => 'required',
            'value_cost' => 'required',
            ],
            [
            'required' => 'Поле :attribute должно быть заполнено',
            'email' => 'Невернный формат поля :attribute',
            'unique'=> 'Данный номер паспорта есть занят',
            'max' => 'Слишком большой комментарий'
            ]);

            $roomer = Roomer::where('id', $request->id)->first();
            $room = Room::where('number', $request->number)->first();

            $dateout = Carbon::parse($request->date_out, "Europe/Moscow");
            $roomerKey;


        if ($request->date_out != $roomer->date_out) {
            $checkRoomers = Roomer::where('number', $room->number)->get()->toArray();
            $i = count($checkRoomers);
            if (empty($i-1)) {
                $roomer->date_out = $request->date_out;
            } else {
                foreach ($checkRoomers as $key => $value) {
                    if ($value['id'] == $roomer->id) {
                         $this->roomerKey = $key;
                    }
                }
                if ($this->roomerKey == $i-1) {
                    $roomer->date_out = $request->date_out;
                } else {
                    $nextDateOut = Carbon::parse($checkRoomers[$this->roomerKey+1]['date_in'], "Europe/Moscow");
                    if ($dateout->diffInMinutes(  $nextDateOut, false)>0) {
                        $roomer->date_out = $request->date_out;
                    } else {
                        return response()->json(['date_out'=>array('Дата выезда клиента больше даты въезда следующего!')], 422);
                    }
                }
            }
        }
        
        if ($request->phone != $roomer->phone) {
             $roomer->phone =  encrypt($request->phone);
        }
        if ($request->name != $roomer->name) {
             $roomer->name =  $request->name;
        }
        if ($request->passport != $roomer->passport) {
             $roomer->passport =  encrypt($request->passport);
        }
        if ($request->address != $roomer->address) {
             $roomer->address =  encrypt($request->address);
        }
        if ($request->coment != $roomer->coment) {
            $roomer->coment = $request->coment;
        }

        $roomer->save();

        return response()->json(['editroomer'=>true],200);
    }

    public function delete(Request $request){

        $roomer = Roomer::where('id',$request->id)->first();
        
        $roomers = Roomer::where('number',$roomer->number)->get();


        if(count($roomers) == 1){
            $room = Room::where('number',$roomer->number)->first();
            $room->free = 0;
            $room->save();
        }


        try{
            $roomer->delete();
            return response()->json(['delete'=>'Клиент удален']);
        }
        catch(Exception $e){
            return response()->json(['errorDel'=>'Ошибка удаленияв']);
        } 
    }

}
