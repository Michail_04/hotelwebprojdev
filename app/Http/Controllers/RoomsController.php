<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Room;
use App\Roomer;;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RoomsController extends Controller
{
    
    private $editParam = array();

    public function rename($rooms)
    {
        foreach ($rooms as $room) {
            if ($room->type == 1) {
                $room->type = ["Стандарт",1];
            }
            if ($room->type == 2) {
                $room->type = ["Полу-люкс",2];
            }
            if ($room->type == 3) {
                $room->type = ["Люкс",3];
            }
            if ($room->free == 0) {
                $room->free = ["Свободно",0];
            }
            if ($room->free == 1) {
                $room->free = ["Занято",1];
            }
        }
        return $rooms;
    }

    public function editRoom(Request $request)
    {
        

           $this->validate($request, [
          'type'=>'required',
          'person'=>'required|numeric'
           ],
           [
           'required' => 'Поле :attribute должно быть заполнено',
           'numeric'=>'Неверный формат'
           ]);


           $id = $request->id;
           $room = DB::table('rooms')->where('id', $id)->get();

        if ($request->cost != $room[0]->cost) {
            $this->validate($request, [
            'cost'=>'required|numeric',
            ],
            [
            'required' => 'Поле :attribute должно быть заполнено',
            'numeric'=>'Неверный формат']);
            
            $this->editParam['cost'] = $request->cost;
        }


        if ($request->number != $room[0]->number) {
            $this->validate($request, [
            'number'=>'required|unique:rooms|numeric',
            ],
            [
            'required' => 'Поле :attribute должно быть заполнено',
            'unique'=> 'Данный адресс занят',
            'numeric'=>'Неверный формат'
            ]);

            $this->editParam['number'] = $request->number;
        }

        if ($request->type[1] == 1 || $request->type[1] == 2 || $request->type[3]) {
            $this->editParam['type'] = $request->type[1];
        }

        if ($request->person != $room[0]->person) {
            $this->editParam['person'] = $request->person;
        }


        DB::table('rooms')->where('id', $room[0]->id)->update($this->editParam);

        
        return response()->json(["editsave" => "Данные обновленны!"], 200);
    }

    public function showEditRoom(Request $request)
    {
 
        if ($request->RoomId) {
            $roomsId = $request->RoomId;
            $rooms = DB::table('rooms')->select('id', 'number', 'type', 'free', 'cost','person')->whereIn('id', $roomsId)->get();

            $rooms = $this->rename($rooms);
        
            return response()->json(['editrooms'=> $rooms]);
        }
        return response()->json(['editerror'=>"Нет данных"], 422);
    }

    public function createRoom(Request $request)
    {
        $this->validate($request, [
        'number'=>'required|unique:rooms|numeric',
        'type'=>'required|numeric',
        'cost'=>'required|numeric'
         ],
         [
         'required' => 'Поле :attribute должно быть заполнено',
         'unique'=> 'Данный номер занят',
         'numeric'=>'Неверный формат'
         ]);

        try {
               $room = new Room;

               $room->person = $request->person;
               $room->number =  $request->number;
               $room->type = $request->type;
               $room->free = 0;
               $room->cost = $request->cost;
                       

               $room->save();
        } catch (Exeption $e) {
            return response()->json([
            'createroom'=>"Не удалось создать комнату"
            ], 422);
        }
            return response()->json([
            'createroom'=>"Комната создана",
            ], 200);
    }
     
    public function filter(Request $request)
    {

        $value =  $request->value;
        if ($value != 4) {
            $rooms= Room::all()
            ->where('type', $value);
            $rooms = $this->rename($rooms);
            return response()->json(['rooms' => $rooms]);
        }
        if ($value == 4) {
            $rooms = Room::all();
            $rooms = $this->rename($rooms);
            return response()->json(["rooms"=>$rooms]);
        }
    }


    public function showfreerooms($type = 4, $person = 4 ){

            $rooms = Room::where('free',0)->get();
            $roomsRoomers = Roomer::get(['date_out','number']);
      
            $Roomers = $roomsRoomers->filter(function ($value, $key){
                $date = Carbon::parse($value->date_out)->day;
                $now = Carbon::now("Europe/Moscow")->day;
                if($date == $now){
                    return true;
                }
                return false;
            });

            $roomsRoomers = Room::whereIn('number', $Roomers->pluck('number'))->get();
       
            if($type != 4){
                $rooms = $rooms->where('type',$type);
                $roomsRoomers = $roomsRoomers->where('type',$type);
            }
            if($person != 4){
                $rooms = $rooms->where('person',$person);
                $roomsRoomers = $roomsRoomers->where('person',$person);
            }

            foreach( $roomsRoomers as $k=>$v){
                $date = Carbon::parse($Roomers[$k]->date_out);
                $time_now =  $date->format('H:i');
                $time = $date->addMinutes(30)->format('H:i');
                $date = $date->format('Y-m-d');           
                $date = ["Освободится в: " .  $time_now,$date,$time];
                $roomsRoomers[$k]->free = $date;
            }
        
            return response()->json(["freeRooms"=> $rooms,"roomsRoomers"=>$roomsRoomers]);
                   
    }

    public function showrooms()
    {      
        $rooms = Room::all();    
        $rooms = $this->rename($rooms);    
        return response()->json(["rooms"=>$rooms]);
    }

    public function delete(Request $request)
    {

        if ($request->deleteRoomId) {
            $arrayWithRoomId = $request->deleteRoomId;
            DB::table('rooms')->whereIn('id', $arrayWithRoomId)->delete();
            return response()->json(['delete'=> "Комната удалена!"]);
        } else {
            return response()->json(['deleteerror'=>'Нет данных']);
        }
    }
}
