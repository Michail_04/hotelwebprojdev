<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use \App\Roomer;
use \App\Room;

class DateController extends Controller
{


    private $Date=array('days'=>array(),'month'=>array(),'rooms'=>array());
    private $idArr = array();
    private $Months = array(
        1=>'Январь',
        2=>'Февраль',
        3=>'Март',
        4=>'Апрель',
        5=>'Май',
        6=>'Июнь',
        7=>'Июль',
        8=>'Август',
        9=>'Сентябрь',
        10=>'Октябрь',
        11=>'Ноябрь',
        12=>'Декабрь'
    );
    private $DaysOfWeek = array(
        0=>array('Вc',1),
        1=>'Пн',
        2=>'Вт',
        3=>'Ср',
        4=>'Чт',
        5=>'Пт',
        6=>array('Сб',1)
    );

    public function convertMonthInStrinf($months)
    {
        
        foreach ($months as $key => $month) {
            foreach ($this->Months as $keym => $value) {
                if ($month == $keym) {
                    $month = array($value,$keym);
                }
            }
             $months[$key] = $month;
        }
        return $months;
    }

    public function getDate($free,$type,$person,$data = null, $flag = null)
    {
        $roomers = Roomer::all();
        $rooms = Room::where('free',$free);

        if($type != 4){
            $rooms = $rooms->where('type',$type);
        }
        if($person != 4){
            $rooms = $rooms->where('person',$person);
        }

        $rooms = $rooms->get();

        if ($data != null) {
            $now = Carbon::parse($data, "Europe/Moscow");
            $now->addDay();
            if ($flag !=null) {
                $now->subDays(21);
            }
        } else {
            $now = Carbon::now("Europe/Moscow");
        }

        
            $nowDay = $now->day;
            $month = $now->month;
            $this->Date['month'][0] = $month;
            $endFor = $nowDay + 20;
            $i =0;
        
        for ($nowDay; $nowDay <  $endFor; $nowDay++) {
            $day = $now->day;
            $month = $now->month;
            $nowYear = $now->year;
            $this->Date['days'][$i][0] = $nowYear;
            $this->Date['days'][$i][1] = $month;
            $this->Date['days'][$i][2] = $day;
            $now->addDay();

            $i++;
        }
        

            $month =  $now->month;
            $this->Date['month'][1] = $month;
            $this->Date['month'] = $this->convertMonthInStrinf($this->Date['month']);
            $nowYear = $now->year;
            $this->Date['year'] =   $nowYear;
          
        foreach ($this->Date['days'] as $key => $value) {
            $date = Carbon::create($value[0], $value[1], $value[2]);
            $this->Date['days'][$key][3] =  $date->toDateString();
            $day = $this->Date['days'][$key][1];
            $this->Date['days'][$key][4] = $this->Months[$day];
            $day = Carbon::parse($this->Date['days'][$key][3]);
            $day = $day->format("w");
            $this->Date['days'][$key][5] = $this->DaysOfWeek[$day];
        }
        
        foreach ($rooms as $key_rooms => $room) {
            foreach ($roomers as $roomers_key => $roomer) {
                if ($roomer->id) {
                    if ($roomer->number == $room->number) {
                        $date_user_in = Carbon::parse($roomer->date_in, "Europe/Moscow");
                        $date_user_out = Carbon::parse($roomer->date_out, "Europe/Moscow");

                        if ($data!=null) {
                            $row_date = Carbon::parse($data, "Europe/Moscow");
                                $row_date->addDay();
                            if ($flag !=null) {
                                $row_date->subDays(21);
                            }
                        } else {
                            $row_date = Carbon::parse($this->Date['days'][0][3]);
                        }
                        for ($b = 0; $b < 20; $b++) {
                            $dateRowlessUserIn =  $date_user_in->diffInDays($row_date, false);
                            $dateRowlessUserOut =  $date_user_out->diffInHours($row_date, false);
                            $row_date->addDay();
                   
                            if ($dateRowlessUserIn >= 0 && $dateRowlessUserOut <= 0) {
                                if (!empty( $this->idArr[$b][1])) {
                                        $this->idArr[$b] = array($this->idArr[$b][1],$roomer->id);
                                }
                                if (!empty( $this->idArr[$b][0])) {
                                        $this->idArr[$b] = array($this->idArr[$b][0],$roomer->id);
                                }
                            
                                if (empty( $this->idArr[$b])) {
                                    if (count($this->idArr) == 0) {
                                        $this->idArr[$b] = $roomer->id;
                                    } elseif (isset($this->idArr[$b-1]) && $this->idArr[$b-1] == 0) {
                                        $this->idArr[$b] = array(0,$roomer->id);
                                    } else {
                                        $this->idArr[$b] = $roomer->id;
                                    }
                                }
                            } else {
                                if (empty($this->idArr[$b])) {
                                     $this->idArr[$b] = 0;
                                    if (isset($this->idArr[$b-1]) && $this->idArr[$b-1] != 0 && !is_array($this->idArr[$b-1])) {
                                        $this->idArr[$b-1] = array($this->idArr[$b-1],0);
                                    }
                                }
                            }
                             $this->Date['rooms'][$key_rooms] = [$room,$this->idArr, $this->Date['days']];
                        }
                    }
                }
            }
            if (empty($this->Date['rooms'][$key_rooms])) {
                for ($b = 0; $b < 20; $b++) {
                    $this->idArr[$b] = 0;
                }
                $this->Date['rooms'][$key_rooms] = [$room,$this->idArr, $this->Date['days']];
            }
            
            $this->idArr = array();
            
            if ($room->type == 1) {
                $room->type = ["Стандарт",1];
            }
            if ($room->type == 2) {
                $room->type = ["Полу-люкс",2];
            }
            if ($room->type == 3) {
                $room->type = ["Люкс",3];
            }
        }
            return response()->json(['date'=>   $this->Date]);
    }
}
