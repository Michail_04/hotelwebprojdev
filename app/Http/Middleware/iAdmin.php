<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use \App\User;

class iAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
         $user = JWTAuth::parseToken()->toUser();

         if($user->rules != 1){
               return response()->json([
                   'iAdmin' => 'У вас недостаточно прав!',
                   'rules' => $user->rules
               ],422);
         }
        return $next($request);
    }
}
