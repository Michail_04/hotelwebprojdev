<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use JWTAuth;
use \App\User;

class RefreshToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $user = JWTAuth::parseToken()->toUser();
        $now = Carbon::now("Europe/Moscow");
        $refreshdata = $user->refreshDatet;
         
        if ($refreshdata) {
            $refreshdata = Carbon::parse($refreshdata);
            $hours =  $now->diffInMinutes($refreshdata);

            if ($hours >= 1) {
                $newToken = JWTAuth::parseToken()->refresh();
                $user->refreshDatet = Carbon::now();
                $user->save();
                return response()->json([
                'auth' => true,
                'refreshToken'=> $newToken,        
                'user_id' => $user->id,
                'user_name' => $user->name,
                'rules' => $user->rules,
                ], 401);
            }
        }
        return  $next($request);
    }
}
