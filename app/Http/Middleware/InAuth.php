<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use \App\User;

class InAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
         $user = JWTAuth::parseToken()->toUser();

         if($user->refreshDatet == null){
               return response()->json([
                   'AuthIn' => 'Обнуление прав, авторизуйтесь заного!',
               ],422);
         }
        return $next($request);
    }
}
