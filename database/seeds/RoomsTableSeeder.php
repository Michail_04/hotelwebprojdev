<?php

use Illuminate\Database\Seeder;

use Faker\Factory;

use App\Room;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public $timestamps = false;
    public function run()
    {

        Room::truncate();

        $a = 0;
        $count = 0;
        foreach (range(1, 20) as $i) {
           if ($a >=3) {
                $a= 1;
            } else {
                $a ++;
            }
              Room::create([
                'number' => $i,
                'type'=> $a,
                'free'=> 0,
                'cost'=> 2000*$a,
                'person'=>$a
             ]);
        }
              
    }
}
