<?php

use Illuminate\Database\Seeder;

use Faker\Factory;

use App\Roomer;
use App\Room;
use Carbon\Carbon;

class RoomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $data;
    public $timestamps = false;
    public function run()
    {

        $faker = Faker\Factory::create('Ru_RU');
        Roomer::truncate();
        $a = 1;
        $day = 1;
        $now = new Carbon("Europe/Moscow");
        foreach (range(1, 10) as $i) {
            $number = $faker->unique()->numberBetween(1, 20);
            $rand = rand(1,5);
              Roomer::create([
                'name' => $faker->name,
                'number'=> $number,
                'phone'=>  encrypt($faker->PhoneNumber),
                'address'=> encrypt($faker->Address),
                'passport'=>encrypt($faker->unique()->randomNumber),
                'value_cost'=>$faker->numberBetween($min = 2000, $max = 15000),
                'coment'=>$faker->text,
                'date_in'=>Carbon::create('2017', '06', $a),
                'date_out'=>Carbon::create('2017', '06', $a*2)
              ]);
             Room::where('number', $number)->update(['free'=>1]);
             $a++;
        }
    }
}
