<?php

use Illuminate\Database\Seeder;

use Faker\Factory;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $timestamps = false;
    public function run(){
        $faker = Faker\Factory::create('Ru_RU');

        User::truncate();


    foreach(range(1, 10) as $i){

            User::create([
                'name' => $faker->name,
                'email'=>$faker->email,
                'password' => bcrypt('test'),
                'rules' => rand(0,1)
            ]);
        }
    }
}
