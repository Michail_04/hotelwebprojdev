<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateRoomersTable extends Migration
{
    public $timestamps = false;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roomers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('passport')->unique();
            $table->text('address');
            $table->text('phone');
            $table->integer('value_cost');
            $table->text('coment')->nullable();
            $table->integer('number');
            $table->dateTime('date_in');
            $table->dateTime('date_out');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roomers');
    }
}
